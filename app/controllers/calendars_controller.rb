class CalendarsController < InheritedResources::Base

  private

    def calendar_params
      params.require(:calendar).permit(:leader, :source)
    end
end

