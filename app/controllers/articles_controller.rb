class ArticlesController < InheritedResources::Base
  before_action :authenticate_user!
  def show
    render json: @article
  end
  def show
    @article = Article.find(params[:id])
  end
  private
    def article_params
      params.require(:article).permit(:title, :description)
    end
end

