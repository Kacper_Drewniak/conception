class FilmsController < InheritedResources::Base
  before_action :authenticate_user!
  def show
    render json: @attach
  end
  private
    def film_params
      params.require(:film).permit(:title, :source)
    end

end
