class AttachesController < InheritedResources::Base
  before_action :authenticate_user!
  def show
    render json: @attach
  end
  def show
    @attach = Attach.find(params[:id])
  end
  private
    def attach_params
      params.require(:attach).permit(:title, :source)
    end

end
