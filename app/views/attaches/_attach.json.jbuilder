json.extract! attach, :id, :title, :source, :created_at, :updated_at
json.url attach_url(attach, format: :json)
