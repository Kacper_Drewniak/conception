ActiveAdmin.register Calendar do
  permit_params :leader, :source
  index do
    column :leader
    column :source
    actions
  end
  show do |a|
    attributes_table do
      row :leader
      row :source
    end
  end
  form do |f|
    f.inputs 'Calendar' do
      f.input :leader
      f.input :source
    end
    f.actions
  end
end
