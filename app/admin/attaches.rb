ActiveAdmin.register Attach do
  permit_params :title, :source
  index do
    column :title
    column :source
    actions
  end
  show do |a|
    attributes_table do
      row :title
      row :source
    end
  end
  form do |f|
    f.inputs 'Attach' do
      f.input :title
      f.input :source, :as => :file
    end
    f.actions
  end
end
