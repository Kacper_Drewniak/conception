ActiveAdmin.register Film do
  permit_params :title, :source
  index do
    column :title
    column :source
    actions
  end
  show do |a|
    attributes_table do
      row :title
      row :source
    end
  end
  form do |f|
    f.inputs 'Attach' do
      f.input :title
      f.input :source
    end
    f.actions
  end
end
