ActiveAdmin.register Article do
  permit_params :title, :description
  index do
    column :title
    column :description
    actions
  end
  show do |a|
    attributes_table do
    row :title
    row :description
    end
  end
  form do |f|
    f.inputs 'Article' do
      f.input :title
      f.input :description, as: :html_editor
    end
    f.actions
  end
end
