Rails.application.routes.draw do

  resources :calendars
  devise_scope :user do
    get '/users/sign_out' => "devise/sessions#destroy"
  end

  get 'calendar/index'
  get 'map/index'

  resources :films
  resources :attaches
  devise_for :users, :controllers => { :sessions => "users/sessions", registrations: "registrations" }
  devise_for :admins, ActiveAdmin::Devise.config
  resources :articles

  begin
    ActiveAdmin.routes(self)
  rescue Exception => e
    puts "ActiveAdmin: #{e.class}: #{e}"
  end
  root to: "home#index"
end
