class AddLeaderToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :patrons, :leader, :string
  end
end
