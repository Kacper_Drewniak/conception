class CreateCalendars < ActiveRecord::Migration[5.0]
  def change
    create_table :calendars do |t|
      t.string :leader
      t.text :source

      t.timestamps
    end
  end
end
